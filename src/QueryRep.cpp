//
//  QueryRep.cpp
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/3/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#include <stdio.h>

//lss
#include "QueryRep.h"

//lemur
#include "BasicDocStream.hpp"
#include "Param.hpp"
#include "TextQuery.hpp"


void
lss::QueryRep::loadQueries(const lemur::api::Index& index)
{
    std::string queFil;
    std::string queryID;
    int count;
    queFil= lemur::api::ParamGetString( "queryFile" );
    if(queFil.length()<=0)
    {
        std::cout<<"The parameter queryFile has not been set."<<std::endl;
        return;
    }
    
    //
    // loading queries from file
    //
    double weight;
    double sumWeight;
    lemur::api::TERM_T term;
    lemur::api::TERMID_T termID;
    
    try{
        /*lemur::api::DocStream* qryStream = new lemur::parse::BasicDocStream( queFil );
        qryStream->startDocIteration();
        count=0;
        while ( qryStream->hasMore() )
        {
            lemur::api::Document* d = qryStream->nextDoc();
            // query terms
            DISTR_T queryTerms;
            queryID = d->getID();
            std::cout<<"****queryID="<<queryID<<std::endl;
            lemur::api::TextQuery* q = new lemur::api::TextQuery( *d );
            sumWeight=0.0;
            q->startTermIteration();
            while(q->hasMore())
            {
                const lemur::api::Term* t = q->nextTerm();
                std::string line = t->spelling();
                std::cout<<"line="<<line<<std::endl;
                term=line;
                weight=1.0;
                termID = index.term( term );
                if ( termID > 0 )
                {
                    queryTerms[termID] = weight;
                    sumWeight += weight;
                }
                else
                {
                    std::cout << "Term '" << term << "' does not exist in index"
                    << " (query term ignored)";
                }
            }
            if ( sumWeight > 0.0 )
            {
                // normalizing term distribution
                DISTR_T queryDistr;
                DISTR_T::const_iterator it;
                
                std::cout << "Internal query representation:";
                
                for ( it = queryTerms.begin(); it != queryTerms.end(); it++ )
                {
                    weight = (double) it->second / sumWeight;
                    queryDistr[it->first] = weight;
                    
                    std::cout << "    TERM: '" << index.term( it->first ) << "'"
                    << " (ID " << it->first << ")   WEIGHT: " << weight;
                }
                
                // recording query
                queryID_.push_back( queryID );
                ptq_.push_back( queryDistr );
            }
            else
            {
                std::cout << "Query '" << queryID << "' is empty; ignored";
            }
            //this->queryID_.push_back(queryID);
        } //while ( qryStream->hasMore() )
        delete qryStream;*/
        // use Lemur's docStream object to parse queries
        lemur::api::DocStream* qryStream = new lemur::parse::BasicDocStream( queFil );
        qryStream->startDocIteration();
        
        while ( qryStream->hasMore() )
        {
            lemur::api::Document* d = qryStream->nextDoc();
            queryID = d->getID();
            //std::cout << "Query '" << queryID << "':"<<std::endl;
            
            // query terms
            DISTR_T queryTerms;
            sumWeight = 0.0;
            lemur::api::TextQuery* q = new lemur::api::TextQuery( *d );
            
            q->startTermIteration();
            while ( q->hasMore() )
            {
                const lemur::api::Term* t = q->nextTerm();
                std::string line = t->spelling();
                int pos = line.find(";");
                
                // query format 1: "term;weight\n"
                if ( pos != string::npos)
                {
                    term = line.substr( 0, pos );
                    char *pEnd;
                    weight = strtod( line.substr( pos+1, line.length() ).c_str(), &pEnd );
                }
                // query format 0: "term\n"
                else
                {
                    term = line;
                    weight = 1.0;
                }
                //std::cout << "  TERM: '" << term << "'   WEIGHT: " << weight;
                ///std::cout<<"term="<<term<<std::endl;
                
                termID = index.term( term );
                /*std::cout<<"document count="<<index.docCount()<<std::endl;
                std::cout<<"term number="<<index.termCount()<<std::endl;
                std::cout<<"Unique term number="<<index.termCountUnique()<<std::endl;
                std::cout<<"termID="<<termID<<std::endl;*/
                if ( termID > 0 )
                {
                    queryTerms[termID] = weight;
                    sumWeight += weight;
                }
                else
                {
                    //std::cout << "Term '" << term << "' does not exist in index" << " (query term ignored)";
                }
            }
            
            if ( sumWeight > 0.0 )
            {
                // normalizing term distribution
                DISTR_T queryDistr;
                DISTR_T::const_iterator it;
                
                for ( it = queryTerms.begin(); it != queryTerms.end(); it++ )
                {
                    weight = (double) it->second / sumWeight;
                    queryDistr[it->first] = weight;
                    
                    //std::cout << "    TERM: '" << index.term( it->first ) << "'" << " (ID " << it->first << ")   WEIGHT: " << weight;
                }      
                
                // recording query
                queryID_.push_back( queryID );			
                ptq_.push_back( queryDistr );
            }
            else
            {
                std::cout << "Query '" << queryID << "' is empty; ignored"<<std::endl;
            }
            
            delete q;
        }
        
        delete qryStream;
    }
    catch( const lemur::api::Exception& e )
    {
        std::cout<<"Can't open query file, check parameter queryFile"<<std::endl;
    } //try
}

const int
lss::QueryRep::queryNum() const
{
    return (int)this->queryID_.size();
}

const std::string
lss::QueryRep::queryID( const int& qID ) const
{
    return queryID_[qID];
}

///
const lss::DISTR_T
lss::QueryRep::query( const int& qID ) const
{
    //if ( qID >= 0 && qID < ptq_.size() )
        return ptq_[qID];
}

///
lemur::retrieval::SimpleKLQueryModel*
lss::QueryRep::query( const lemur::api::Index& index, const int& qID ) const
{
    lemur::retrieval::SimpleKLQueryModel* qModel
    = new lemur::retrieval::SimpleKLQueryModel( index );
    
    const DISTR_T& q = this->query( qID );
    for ( DISTR_T::const_iterator term_it = q.begin();
         term_it != q.end(); term_it++ ) {
        const lemur::api::TERMID_T& termID = term_it->first; // t
        const double& ptq = term_it->second; // p(t|q)
        qModel->setCount( termID, ptq );
    }
    
    return qModel;
}

void
lss::QueryRep::display(const lemur::api::Index& index)
{
    /*
     private:
     std::vector< std::string > queryID_;
     /// Queries
     /// (i.e., ptq_[q] holds p(t|q) for query q)
     std::vector< DISTR_T > ptq_;
     */
    for(unsigned long i=0; i<queryID_.size(); i++)
    {
        std::cout<<"****"<<queryID_[i]<<std::endl;
    }
    for(unsigned long i=0; i<ptq_.size(); i++)
    {
        for(DISTR_T::iterator it=ptq_[i].begin(); it!=ptq_[i].end(); it++)
        {
            std::cout<<"+++++"<<it->first<<"  "<<index.term(it->first)<<"  "<<this->queryID(i)<<" "<<it->second<<std::endl;
        }
    }
}




















