//
//  MRR.cpp
//  diversify
//
//  Created by Shangsong Liang on 7/23/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#include "MRR.h"

//Lemur


/*lss::MRR::MRR()
{
    //this->openIndex();
}*/

lss::MRR::~MRR()
{
    //this->closeIndex();
}

/*void
lss::MRR::openIndex()
{
    std::string indFil=lemur::api::ParamGetString("index");
    if(indFil.length()<=0)
    {
        std::cout<<"Parameter 'index' is not set."<<std::endl;
        return;
    }
    try
    {
        index_=lemur::api::IndexManager::openIndex(indFil);
    }
    catch (const lemur::api::Exception& e)
    {
        std::cout<<"Can't open index, check parameter 'index'. "<<std::endl;
    }
    this->smoMet_=lemur::api::ParamGetString("smoothing");
    if(this->smoMet_.length()<=0)
    {
        std::cout<<"Parameter 'smoothing' is not set."<<std::endl;
        return;
    }
    lemur::api::ParamGet("smoothingPar", this->smoPar_, 1000);
    lemur::api::ParamGet("lambdaMMR", this->lambdaMMR_, 0.5);
    //std::cout<<index_->docCount()<<std::endl;
    collCounter_ = new lemur::langmod::DocUnigramCounter(*index_);
    collModel_ = new lemur::langmod::MLUnigramLM( *collCounter_, index_->termLexiconID() );
    std::cout<<"555555555"<<std::endl;
}

void
lss::MRR::closeIndex()
{
    if(index_!=NULL)
    {
        delete index_;
        index_=0;
    }
    if(collCounter_!=NULL)
    {
        delete collCounter_;
    }
    
    collModel_=0;
}*/

void
lss::MRR::processMRR(const std::string que, lss::DOCSCO_T docSco)
{
    
}

/*void
lss::MRR::processMRR(const std::string que, lss::DOCSCO_T docSco)
{
    const int numOfDoc=docSco.size();
    lss::QUEDOCSCO_T::iterator qDS_it;
    lss::DOCSCO_T dS_S;
    lss::DOCSCO_T::iterator dS_S_it;
    lss::DOCSCO_T dS_R;
    lss::DOCSCO_T::iterator dS_R_it;
    lss::DOCSCO_T::iterator docSco_it;
    lss::DOCSCO_T docInd; //document-index mapping
    lss::DOCINDDOC_T indDoc; //index-document mapping
    lss::DOCINDDOC_T::iterator indDoc_it;
    lss::DOCSCO_T::iterator docInd_it;
    std::string docStr;
    double fusSco_max=0.0;
    double sim1=0.0;
    double sim2=0.0;
    double sim2_max=0.0;
    double mmr_max=0.0;
    double mmr=0.0;
    int index_i=0;
    int index_d=0;
    int temp_int=0;
    
    int docID1=0;
    int docID2=0;
    double docLen1=0.0;
    double docLen2=0.0;
    double skLScor=0.0;//symmetric K-L divergence score
    int termID=0;
    double prob=0.0;
    double probSum=0.0;
    double smoPro1=0.0;//smoothing p(w|Q)
    double smoPro2=0.0;//smoothing p(w|D)
    double muPwC=0.0;//mu*p(w|c)
    double sumWInQ=0.0;//term in Q
    double sumWInDNotInQ=0.0;//term in D but not in Q
    double sumWOutQAndD=0.0;// term in the collection, but not in both Q and D (outside both Q and D)
    std::string termStr;
    
    lemur::api::ParamGet("smoothingPar", this->smoPar_, 1000);
    lemur::api::ParamGet("lambdaMMR", this->lambdaMMR_, 0.5);
    lemur::api::ParamGet("topKMMR", this->topK_, 50);
    
    qDS_it=this->qDS.find(que);
    if(qDS_it!=this->qDS.end())
    {
        std::cout<<que<<" has been processed."<<std::endl;
        return;
    }
    for(docSco_it=docSco.begin(); docSco_it!=docSco.end(); docSco_it++)
    {
        if(fusSco_max<docSco_it->second)
        {
            fusSco_max=docSco_it->second;
        }
    }
    dS_R=docSco;
    dS_S.clear();
    for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
    {
        dS_R_it->second=dS_R_it->second/fusSco_max;
    }
    temp_int=0;
    for(docInd_it=docSco.begin(); docInd_it!=docSco.end(); docInd_it++)
    {
        docInd.insert(lss::DOCSCO_PAIR_T(docInd_it->first, temp_int));
        indDoc.insert(lss::DOCINDDOC_PAIR_T(temp_int, docInd_it->first));
        temp_int=temp_int+1;
    }
    std::cout<<"total number ="<<index_->docCount()<<std::endl;
    //indexP_=&index_;
    lemur::langmod::DocUnigramCounter* collCounter=new lemur::langmod::DocUnigramCounter(*index_);
    lemur::langmod::UnigramLM *collModel= new lemur::langmod::MLUnigramLM( *collCounter, index_->termLexiconID() );
    for(int i=0; i<numOfDoc; i++)
    {
        mmr_max=-1.0;
        for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        {
            docID1=index_->document(dS_R_it->first);
            docLen1=index_->docLength(docID1);
            //lemur::langmod::DocUnigramCounter* collCounter1
            //= new lemur::langmod::DocUnigramCounter(docID1,index_ );
            lemur::langmod::DocUnigramCounter collCounter1(docID1, *index_);
            //lemur::langmod::UnigramLM* collModel1
            //= new lemur::langmod::MLUnigramLM( *collCounter1, index_.termLexiconID() );
            lemur::langmod::MLUnigramLM collModel1(collCounter1, index_->termLexiconID());
            for(dS_S_it=dS_S.begin(); dS_S_it!=dS_S.end(); dS_S_it++)
            {
                docID2=index_->document(dS_S_it->first);
                docLen2=index_->docLength(docLen2);
                //lemur::langmod::DocUnigramCounter* collCounter2
                //= new lemur::langmod::DocUnigramCounter(docID2, index_);
                lemur::langmod::DocUnigramCounter collCounter2(docID2, *index_);
                //lemur::langmod::UnigramLM *collModel2
                //= new lemur::langmod::MLUnigramLM( *collCounter2, index_.termLexiconID() );
                lemur::langmod::MLUnigramLM collModel2(collCounter2, index_->termLexiconID());
                collModel1.startIteration();
                for(;collModel1.hasMore();)
                {
                    collModel1.nextWordProb(termID, prob);
                    muPwC=this->smoPar_*collModel->prob(termID);
                    if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
                    {
                        smoPro1=(prob*docLen1+muPwC)/(docLen1+this->smoPar_);
                        smoPro2=(collModel2.prob(termID)*docLen2+muPwC)/(docLen2+this->smoPar_);
                        sumWInQ=sumWInQ+smoPro1*(log(smoPro1/smoPro2));
                    }
                }
                skLScor=sumWInQ;
                collModel2.startIteration();
                for(;collModel2.hasMore();)
                {
                    collModel2.nextWordProb(termID, prob);
                    muPwC=this->smoPar_*collModel->prob(termID);
                    if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
                    {
                        smoPro1=(collModel1.prob(termID)*docLen1+muPwC)/(docLen1+this->smoPar_);
                        smoPro2=(prob*docLen2+muPwC)/(docLen2+this->smoPar_);
                        sumWInDNotInQ=sumWInDNotInQ+smoPro2*log(smoPro2/smoPro1);
                    }
                }
                skLScor=skLScor+sumWInDNotInQ;
                sim2=exp(-0.5*skLScor);
                if(sim2_max<sim2)
                {
                    sim2_max=sim2;
                }
                //delete collCounter2;
                //collCounter2=0;
                //delete collModel2;
                //collModel2=0;
            }//for(dS_S_it=dS_S.begin(); dS_S_it!=dS_S.end(); dS_S_it++)
            //delete collCounter1;
            //collCounter1=0;
            //delete collModel1;
            //collModel1=0;
            sim1=dS_R_it->second/fusSco_max;
            mmr=lambdaMMR_*(sim1 - (1.0-lambdaMMR_)*sim2_max);
            if(mmr_max<mmr)
            {
                mmr_max=mmr;
                docStr=dS_R_it->first;
                //docInd_it=docInd.find(dS_R_it->first);
            }
        }//for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        dS_S.insert(lss::DOCSCO_PAIR_T(docStr, numOfDoc-i));
        std::cout<<que<<" "<<docStr<<" "<<skLScor<<std::endl;//**
        dS_R.erase(docStr);
    }//for(int i=0; i<numOfDoc; i++)
    delete collCounter;
    collModel=0;
    this->qDS.insert(lss::QUEDOCSCO_PAIR_T(que, dS_S));
}*/

lss::DOCSCO_T
lss::MRR::processMRR_wor(const std::string que, lss::DOCSCO_T dS)
{
    int temp_int, sumTok;
    double mmr_max;
    double mmr_mix;
    double sco_max;
    double score;
    double sim1, mmr, sim2;
    double sim2_max=0.0;
    double sim2_max_min;
    std::string str;
    std::string subStr;
    std::string queDocDir;
    std::string filStr;
    std::string dStr;
    std::string docStr;
    std::string docStr1, docStr2;
    std::fstream input;
    unsigned long first;
    unsigned long index1, index2;
    std::vector<std::string> tokens;
    lss::DOCTOKSCO_T dTS;
    lss::DOCTOKSCO_T::iterator dTS_it1, dTS_it2;
    lss::TOKSCO_T::iterator tokSco_it1, tokSco_it2, tokSco_it;
    lss::DOCSCO_T dS_S;
    lss::DOCSCO_T::iterator dS_S_it;
    lss::DOCSCO_T dS_R;
    lss::DOCSCO_T::iterator dS_R_it;
    lss::TOKSCO_T tS;
    lss::DOCSCO_T::iterator dS_it;
    lss::Processing proc(index_);
    const int numOfDoc=(int)dS.size();
    
    queDocDir=lemur::api::ParamGetString("forQueDocDirMrr", "");
    lemur::api::ParamGet("lambdaMMR", this->lambdaMMR_, 0.5);
    if(queDocDir.length()<=0)
    {
        std::cout<<"The parameter queDocDir has not been set."<<std::endl;
    }
    filStr=queDocDir+"/"+que;
    input.open(filStr.c_str(), std::ios::in);
    if(!input)
    {
        std::cout<<"Can not open the file: "<<filStr<<std::endl;
        return dS_S;
    }
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=0)
        {
            continue;
        }
        first=str.find(' ', 0);
        if(first>=str.length()-1)
        {
            continue;
        }
        dStr=str.substr(0, first);
        subStr=str.substr(first+1, str.length()-first-1);
        std::istringstream iss(subStr);
        tokens.clear();
        std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(tokens));
        tS.clear();
        for(unsigned long i=0; i<tokens.size(); i++)
        {
            tS[tokens[i]]=1.0;
        }
        dTS[dStr]=tS;
    }
    dS_R=dS;
    dS_S.clear();
    sco_max=0.0;
    for(dS_it=dS.begin(); dS_it!=dS.end(); dS_it++)
    {
        if(dS_it->second>sco_max)
        {
            sco_max=dS_it->second;
        }
    }
    for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
    {
        dS_R_it->second=dS_R_it->second/sco_max;
    }
    temp_int=0;
    for(int i=0; i<numOfDoc; i++)
    {
        mmr_max=-1.0;
        mmr_mix=10000;
        sim2_max_min=10000;
        for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        {
            docStr1=dS_R_it->first;
            dTS_it1=dTS.find(docStr1);
            sim2_max=0.0;
            for(dS_S_it=dS_S.begin(); dS_S_it!=dS_S.end(); dS_S_it++)
            {
                docStr2=dS_S_it->first;
                dTS_it2=dTS.find(docStr2);
                sumTok=0;
                for(tokSco_it2=dTS_it2->second.begin(); tokSco_it2!=dTS_it2->second.end(); tokSco_it2++)
                {
                    tokSco_it=dTS_it1->second.find(tokSco_it2->first);
                    if(tokSco_it!=dTS_it1->second.end())
                    {
                        sumTok=sumTok+1;
                    }
                }
                sim2=sumTok*1.0/(dTS_it1->second.size() * dTS_it2->second.size());
                if(sim2>sim2_max)
                {
                    sim2_max=sim2;
                }
            } //for(dS_S_it=dS_S.begin();
            sim1=dS_R_it->second;
            mmr=lambdaMMR_*(sim1 - (1.0-lambdaMMR_)*sim2_max);
            if(mmr_max<mmr)
            {
                mmr_max=mmr;
                docStr=dS_R_it->first;
                score=mmr;
                //std::cout<<"sim1="<<sim1<<" sim2_max="<<sim2_max<<std::endl;
                //docInd_it=docInd.find(dS_R_it->first);
            }
        }
        //dS_S.insert(lss::DOCSCO_PAIR_T(docStr, numOfDoc-i));// or dS_S.insert(lss::DOCSCO_PAIR_T(docStr, score));
        dS_S.insert(lss::DOCSCO_PAIR_T(docStr, score));
        dS_R.erase(docStr);
    } //for(int i=0; i<numOfDoc; i++)
    return dS_S;
}

/*
void
lss::MRR::processMRR0(const std::string que, lss::DOCSCO_T docSco)
{
    const int numOfDoc=docSco.size();
    lss::QUEDOCSCO_T::iterator qDS_it;
    lss::DOCSCO_T dS_S;
    lss::DOCSCO_T::iterator dS_S_it;
    lss::DOCSCO_T dS_R;
    lss::DOCSCO_T::iterator dS_R_it;
    lss::DOCSCO_T::iterator docSco_it;
    lss::DOCSCO_T docInd; //document-index mapping
    lss::DOCINDDOC_T indDoc; //index-document mapping
    lss::DOCINDDOC_T::iterator indDoc_it;
    lss::DOCSCO_T::iterator docInd_it;
    std::string docStr;
    double fusSco_max=0.0;
    double sim1=0.0;
    double sim2=0.0;
    double sim2_max=0.0;
    double mmr_max=0.0;
    double mmr=0.0;
    int index_i=0;
    int index_d=0;
    int temp_int=0;
    
    int docID1=0;
    int docID2=0;
    double docLen1=0.0;
    double docLen2=0.0;
    double skLScor=0.0;//symmetric K-L divergence score
    int termID=0;
    double prob=0.0;
    double probSum=0.0;
    double smoPro1=0.0;//smoothing p(w|Q)
    double smoPro2=0.0;//smoothing p(w|D)
    double muPwC=0.0;//mu*p(w|c)
    double sumWInQ=0.0;//term in Q
    double sumWInDNotInQ=0.0;//term in D but not in Q
    double sumWOutQAndD=0.0;// term in the collection, but not in both Q and D (outside both Q and D)
    std::string termStr;
    
    qDS_it=this->qDS.find(que);
    if(qDS_it!=this->qDS.end())
    {
        std::cout<<que<<" has been processed."<<std::endl;
        return;
    }
    for(docSco_it=docSco.begin(); docSco_it!=docSco.end(); docSco_it++)
    {
        if(fusSco_max<docSco_it->second)
        {
            fusSco_max=docSco_it->second;
        }
    }
    dS_R=docSco;
    dS_S.clear();
    for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
    {
        dS_R_it->second=dS_R_it->second/fusSco_max;
    }
    temp_int=0;
    for(docInd_it=docSco.begin(); docInd_it!=docSco.end(); docInd_it++)
    {
        docInd.insert(lss::DOCSCO_PAIR_T(docInd_it->first, temp_int));
        indDoc.insert(lss::DOCINDDOC_PAIR_T(temp_int, docInd_it->first));
        temp_int=temp_int+1;
    }
    lemur::langmod::DocUnigramCounter* collCounter
    =new lemur::langmod::DocUnigramCounter(*index_);
    lemur::langmod::UnigramLM *collModel
    = new lemur::langmod::MLUnigramLM( *collCounter, index_->termLexiconID() );
    for(int i=0; i<numOfDoc; i++)
    {
        mmr_max=-1.0;
        for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        {
            docID1=index_->document(dS_R_it->first);
            docLen1=index_->docLength(docID1);
            lemur::langmod::DocUnigramCounter* collCounter1
            = new lemur::langmod::DocUnigramCounter(docID1,*index_ );
            lemur::langmod::UnigramLM* collModel1
            = new lemur::langmod::MLUnigramLM( *collCounter1, index_->termLexiconID() );
            for(dS_S_it=dS_S.begin(); dS_S_it!=dS_S.end(); dS_S_it++)
            {
                docID2=index_->document(dS_S_it->first);
                docLen2=index_->docLength(docLen2);
                lemur::langmod::DocUnigramCounter* collCounter2
                = new lemur::langmod::DocUnigramCounter(docID2, *index_);
                lemur::langmod::UnigramLM *collModel2
                = new lemur::langmod::MLUnigramLM( *collCounter2, index_->termLexiconID() );
                collModel1->startIteration();
                for(;collModel1->hasMore();)
                {
                    collModel1->nextWordProb(termID, prob);
                    muPwC=this->smoPar_*collModel->prob(termID);
                    if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
                    {
                        smoPro1=(prob*docLen1+muPwC)/(docLen1+this->smoPar_);
                        smoPro2=(collModel2->prob(termID)*docLen2+muPwC)/(docLen2+this->smoPar_);
                        sumWInQ=sumWInQ+smoPro1*(log(smoPro1/smoPro2));
                    }
                }
                skLScor=sumWInQ;
                collModel2->startIteration();
                for(;collModel2->hasMore();)
                {
                    collModel2->nextWordProb(termID, prob);
                    muPwC=this->smoPar_*collModel->prob(termID);
                    if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
                    {
                        smoPro1=(collModel1->prob(termID)*docLen1+muPwC)/(docLen1+this->smoPar_);
                        smoPro2=(prob*docLen2+muPwC)/(docLen2+this->smoPar_);
                        sumWInDNotInQ=sumWInDNotInQ+smoPro2*log(smoPro2/smoPro1);
                    }
                }
                skLScor=skLScor+sumWInDNotInQ;
                sim2=exp(-0.5*skLScor);
                if(sim2_max<sim2)
                {
                    sim2_max=sim2;
                }
                delete collCounter2;
                collCounter2=0;
                delete collModel2;
                collModel2=0;
            }//for(dS_S_it=dS_S.begin(); dS_S_it!=dS_S.end(); dS_S_it++)
            delete collCounter1;
            collCounter1=0;
            delete collModel1;
            collModel1=0;
            sim1=dS_R_it->second/fusSco_max;
            mmr=lambdaMMR_*(sim1 - (1.0-lambdaMMR_)*sim2_max);
            if(mmr_max<mmr)
            {
                mmr_max=mmr;
                docStr=dS_R_it->first;
                //docInd_it=docInd.find(dS_R_it->first);
            }
        }//for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        dS_S.insert(lss::DOCSCO_PAIR_T(docStr, numOfDoc-i));
        dS_R.erase(docStr);
    }//for(int i=0; i<numOfDoc; i++)
    delete collCounter;
    collModel=0;
    this->qDS.insert(lss::QUEDOCSCO_PAIR_T(que, dS_S));
}*/

lss::QUEDOCSCO_T
lss::MRR::getQDS()
{
    return this->qDS;
}

/*
void
lss::MRR::processMRRUsingTex(const std::string que, lss::DOCSCO_T docSco)
{
    const int numOfDoc=(int)docSco.size();
    lss::QUEDOCSCO_T::iterator qDS_it;
    lss::DOCSCO_T dS_S;
    lss::DOCSCO_T::iterator dS_S_it;
    lss::DOCSCO_T dS_R;
    lss::DOCSCO_T::iterator dS_R_it;
    lss::DOCSCO_T::iterator docSco_it;
    lss::DOCSCO_T docInd; //document-index mapping
    lss::DOCINDDOC_T indDoc; //index-document mapping
    lss::DOCINDDOC_T::iterator indDoc_it;
    lss::DOCSCO_T::iterator docInd_it;
    std::vector<lss::TERFRE_T> dTF; //doc-term-frequency
    lss::TERFRE_T::iterator tF_it;
    lss::TERFRE_T::iterator tF_it1;
    lss::TERFRE_T::iterator tF_it2;
    std::string docStr;
    std::string dirStr;
    std::string ldaForDocDir;
    std::string str;
    std::string terStr;
    std::string filStr;
    double fusSco_max=0.0;
    double sim1=0.0;
    double sim2=0.0;
    double sim2_max=0.0;
    double mmr_max=0.0;
    double mmr=0.0;
    int index_R=0;
    int index_S=0;
    int temp_int=0;
    int index_pre=0;
    int index_now=0;
    std::fstream input;
    
    double smoPro1=0.0;//smoothing p(w|Q)
    double smoPro2=0.0;//smoothing p(w|D)
    double docLen1=0.0;
    double docLen2=0.0;
    double sum1=0.0;
    double sum2=0.0;
    int termID=0;
    int docID1=0;
    int docID2=0;
    
    qDS_it=this->qDS.find(que);
    if(qDS_it!=this->qDS.end())
    {
        std::cout<<que<<" has been processed."<<std::endl;
        return;
    }
    ldaForDocDir=lemur::api::ParamGetString("ldaForDocDir");
    if(ldaForDocDir.length()<=0)
    {
        std::cout<<"The parameter 'ldaForDocDir' has not been set."<<std::endl;
        return;
    }
    dTF.clear();
    for(docSco_it=docSco.begin(); docSco_it!=docSco.end(); docSco_it++)
    {
        lss::TERFRE_T tF;
        docStr=docSco_it->first;
        dirStr=docStr.substr(10, 6);
        if(dirStr.compare("en0011")>0 && dirStr.compare("enwp00")<0)
        {
            dirStr="enOther";
        }
        filStr=ldaForDocDir+"/"+dirStr+"/"+docStr;
        input.open(filStr.c_str(), std::ios::in);
        if(!input)
        {
            std::cout<<"Can not read the file: "<<filStr<<std::endl;
            return;
        }
        std::getline(input, str);
        index_pre=0;
        for(int i=0; i<str.length(); i++)
        {
            if(str[i]==' ')
            {
                terStr=str.substr(index_pre, i-index_pre);
                index_pre=i+1;
                tF_it=tF.find(terStr);
                if(tF_it==tF.end())
                {
                    tF.insert(lss::TERFRE_PAIR_T(terStr, 1.0));
                }
                else
                {
                    tF_it->second=tF_it->second+1.0;
                }
            }
        }//for(int i=0; i<str.length(); i++)
        input.close();
        dTF.push_back(tF);
    }//for(docSco_it=docSco.begin(); docSco_it!=docSco.end(); docSco_it++)
    for(docSco_it=docSco.begin(); docSco_it!=docSco.end(); docSco_it++)
    {
        if(fusSco_max<docSco_it->second)
        {
            fusSco_max=docSco_it->second;
        }
    }
    dS_R=docSco;
    dS_S.clear();
    for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
    {
        dS_R_it->second=dS_R_it->second/fusSco_max;
    }
    temp_int=0;
    for(docInd_it=docSco.begin(); docInd_it!=docSco.end(); docInd_it++)
    {
        docInd.insert(lss::DOCSCO_PAIR_T(docInd_it->first, temp_int));
        indDoc.insert(lss::DOCINDDOC_PAIR_T(temp_int, docInd_it->first));
        temp_int=temp_int+1;
    }
    for(int i=0; i<numOfDoc; i++)
    {
        mmr_max=-1.0;
        for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        {
            docInd_it=docInd.find(dS_R_it->first);
            index_R=(int)docInd_it->second;
            docID1=index_->document(dS_R_it->first);
            docLen1=index_->docLength(docID1);
            sim2_max=0.0;
            for(dS_S_it=dS_S.begin(); dS_S_it!=dS_S.end(); dS_S_it++)
            {
                docInd_it=docInd.find(dS_S_it->first);
                index_S=(int)docInd_it->second;
                docID2=index_->document(dS_S_it->first);
                docLen2=index_->docLength(docID2);
                sum1=0.0;
                for(tF_it1=dTF[index_R].begin(); tF_it1!=dTF[index_R].end(); tF_it1++)
                {
                    termID=index_->term(tF_it1->first);
                    smoPro1=(tF_it1->second+this->smoPar_*collModel_->prob(termID))/(docLen1+this->smoPar_);
                    tF_it2=dTF[index_S].find(tF_it1->first);
                    if(tF_it2==dTF[index_S].end())
                    {
                        smoPro2=this->smoPar_*collModel_->prob(termID)/(docLen2+this->smoPar_);
                    }
                    else
                    {
                        smoPro2=(tF_it2->second+this->smoPar_*collModel_->prob(termID))/(docLen2+this->smoPar_);
                    }
                    sum1=sum1+smoPro1*log(smoPro1/smoPro2);
                }//for(tF_it1=dTF[index_R].begin(); tF_it1!=dTF[index_R].end(); tF_it1++)
                sum2=0.0;
                for(tF_it2=dTF[index_S].begin(); tF_it2!=dTF[index_S].end(); tF_it2++)
                {
                    termID=index_->term(tF_it2->first);
                    smoPro2=(tF_it2->second+this->smoPar_*collModel_->prob(termID))/(docLen2+this->smoPar_);
                    tF_it1=dTF[index_R].find(tF_it2->first);
                    if(tF_it1==dTF[index_R].end())
                    {
                        smoPro1=this->smoPar_*collModel_->prob(termID)/(docLen1+this->smoPar_);
                    }
                    else
                    {
                        smoPro1=(tF_it1->second+this->smoPar_*collModel_->prob(termID))/(docLen1+this->smoPar_);
                    }
                    sum2=sum2+smoPro2*log(smoPro2/smoPro1);
                }
                sim2=exp(-0.5*(sum1+sum2));
                if(sim2>sim2_max)
                {
                    sim2_max=sim2;
                }
            }//for(dS_S_it=dS_S.begin(); dS_S_it!=dS_S.end(); dS_S_it++)
            sim1=dS_R_it->second/fusSco_max;
            mmr=lambdaMMR_*(sim1 - (1.0-lambdaMMR_)*sim2_max);
            if(mmr_max<mmr)
            {
                mmr_max=mmr;
                docStr=dS_R_it->first;
                //docInd_it=docInd.find(dS_R_it->first);
            }
        }//for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        dS_S.insert(lss::DOCSCO_PAIR_T(docStr, numOfDoc-i));
        dS_R.erase(docStr);
    }//for(int i=0; i<numOfDoc; i++)
    this->qDS.insert(lss::QUEDOCSCO_PAIR_T(que, dS_S));
}*/

void
lss::MRR::retMod() // the structure of the data must be qID/date/filNam
{
    std::string str;
    std::string mrrDir;
    std::string queStr;
    std::string datStr;
    std::string filStr;
    std::string patStr;
    std::string fulFilNam; // the full fill name
    std::string mrrResDir;
    std::string cmdStr;
    std::string queDocFil, thetaFil;
    std::string queDocDir;
    std::string rankFil;
    std::string mrrRankFilNam;
    unsigned long first;
    unsigned long second;
    string::size_type idx;
    std::fstream input;
    std::fstream input2;
    lss::Processing proc(index_);
    //lss::Processing proc2;
    lss::DOCSCO_T dS;
    lss::DOCSCO_T::iterator it;
    
    mrrDir=lemur::api::ParamGetString("other_mrrDir", "");
    mrrResDir=lemur::api::ParamGetString("mrrResDir", "");
    queDocDir=lemur::api::ParamGetString("queDocDir", "");
    mrrRankFilNam=lemur::api::ParamGetString("mrrRankFil", "");
    //cmdStr="rm -r "+mrrResDir+"/*";
    std::system(cmdStr.c_str());
    //std::cout<<"mrrDir="<<mrrDir<<std::endl;
    //std::cout<<"mrrResDir="<<mrrResDir<<std::endl;
    //std::cout<<"queDocDir="<<queDocDir<<std::endl;
    lemur::api::ParamGet("topKMMR", this->topK_, 50);
    proc.itePriFil(mrrDir);
    for(unsigned long i=0; i<proc.filVec.size(); i++)
    {
        lss::DOCSCO_T dS_res;
        fulFilNam=proc.filVec[i];
        filStr=proc.getFilNam(fulFilNam);
        patStr=proc.getPat(fulFilNam);
        first=filStr.find(".theta");
        if(first>=filStr.length())
        {
            continue;
        }
        datStr=proc.getFilNam(patStr);
        patStr=proc.getPat(patStr);
        queStr=proc.getFilNam(patStr);
        patStr=proc.getPat(patStr);
        //std::cout<<proc.filVec[i]<<std::endl;
        //std::cout<<"query="<<queStr<<" patStr="<<datStr<<std::endl;
        str=mrrResDir+"/"+queStr;
        input.open(str.c_str(), std::ios::in);
        if(!input)
        {
            cmdStr="mkdir "+str;
            std::system(cmdStr.c_str());
        }
        else
        {
            input.close();
        }
        str=mrrResDir+"/"+queStr+"/"+datStr;
        rankFil=str+"/"+datStr+"."+mrrRankFilNam;
        input2.open(str.c_str(), std::ios::in);
        if(!input2)
        {
            cmdStr="mkdir "+str;
            std::system(cmdStr.c_str());
        }
        else
        {
            input2.close();
        }
        queDocFil=queDocDir+"/"+queStr;
        thetaFil=fulFilNam;
        //std::cout<<"forQueFil="<<queDocFil<<"   thetaFil="<<thetaFil<<std::endl;
        dS=proc.getDocSco(queDocFil, thetaFil, topK_);
        //std::cout<<"queDocFil="<<queDocFil<<" thetaFil="<<thetaFil<<std::endl;
        //std::cout<<"size=**="<<dS.size()<<" date="<<datStr<<std::endl;
        //this->processMRR(queStr, dS);
        dS_res=this->processMRR_wor(queStr, dS);
        resWri_.writeRank(queStr, dS_res, rankFil, mrrRankFilNam);
        
    } // for(unsigned long i=0;
    
}

/*void
lss::MRR::processMRRUsingTex(const std::string que, lss::DOCSCO_T docSco)
{
    const int numOfDoc=docSco.size();
    lss::QUEDOCSCO_T::iterator qDS_it;
    lss::DOCSCO_T dS_S;
    lss::DOCSCO_T::iterator dS_S_it;
    lss::DOCSCO_T dS_R;
    lss::DOCSCO_T::iterator dS_R_it;
    lss::DOCSCO_T::iterator docSco_it;
    lss::DOCSCO_T docInd; //document-index mapping
    lss::DOCINDDOC_T indDoc; //index-document mapping
    lss::DOCINDDOC_T::iterator indDoc_it;
    lss::DOCSCO_T::iterator docInd_it;
    std::vector<lss::TERFRE_T> dTF; //doc-term-frequency
    lss::TERFRE_T::iterator tF_it;
    lss::TERFRE_T::iterator tF_it1;
    lss::TERFRE_T::iterator tF_it2;
    std::string docStr;
    std::string dirStr;
    std::string ldaForDocDir;
    std::string str;
    std::string terStr;
    std::string filStr;
    double fusSco_max=0.0;
    double sim1=0.0;
    double sim2=0.0;
    double sim2_max=0.0;
    double mmr_max=0.0;
    double mmr=0.0;
    int index_R=0;
    int index_S=0;
    int temp_int=0;
    int index_pre=0;
    int index_now=0;
    std::fstream input;
    
    double smoPro1=0.0;//smoothing p(w|Q)
    double smoPro2=0.0;//smoothing p(w|D)
    double docLen1=0.0;
    double docLen2=0.0;
    double sum1=0.0;
    double sum2=0.0;
    int termID=0;
    int docID1=0;
    int docID2=0;
    
    qDS_it=this->qDS.find(que);
    if(qDS_it!=this->qDS.end())
    {
        std::cout<<que<<" has been processed."<<std::endl;
        return;
    }
    ldaForDocDir=lemur::api::ParamGetString("ldaForDocDir");
    if(ldaForDocDir.length()<=0)
    {
        std::cout<<"The parameter 'ldaForDocDir' has not been set."<<std::endl;
        return;
    }
    dTF.clear();
    for(docSco_it=docSco.begin(); docSco_it!=docSco.end(); docSco_it++)
    {
        lss::TERFRE_T tF;
        docStr=docSco_it->first;
        dirStr=docStr.substr(10, 6);
        if(dirStr.compare("en0011")>0 && dirStr.compare("enwp00")<0)
        {
            dirStr="enOther";
        }
        filStr=ldaForDocDir+"/"+dirStr+"/"+docStr;
        input.open(filStr.c_str(), std::ios::in);
        if(!input)
        {
            std::cout<<"Can not read the file: "<<filStr<<std::endl;
            return;
        }
        std::getline(input, str);
        index_pre=0;
        for(int i=0; i<str.length(); i++)
        {
            if(str[i]==' ')
            {
                terStr=str.substr(index_pre, i-index_pre);
                index_pre=i+1;
                tF_it=tF.find(terStr);
                if(tF_it==tF.end())
                {
                    tF.insert(lss::TERFRE_PAIR_T(terStr, 1.0));
                }
                else
                {
                    tF_it->second=tF_it->second+1.0;
                }
            }
        }//for(int i=0; i<str.length(); i++)
        input.close();
        dTF.push_back(tF);
    }//for(docSco_it=docSco.begin(); docSco_it!=docSco.end(); docSco_it++)
    for(docSco_it=docSco.begin(); docSco_it!=docSco.end(); docSco_it++)
    {
        if(fusSco_max<docSco_it->second)
        {
            fusSco_max=docSco_it->second;
        }
    }
    dS_R=docSco;
    dS_S.clear();
    for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
    {
        dS_R_it->second=dS_R_it->second/fusSco_max;
    }
    temp_int=0;
    for(docInd_it=docSco.begin(); docInd_it!=docSco.end(); docInd_it++)
    {
        docInd.insert(lss::DOCSCO_PAIR_T(docInd_it->first, temp_int));
        indDoc.insert(lss::DOCINDDOC_PAIR_T(temp_int, docInd_it->first));
        temp_int=temp_int+1;
    }
    for(int i=0; i<numOfDoc; i++)
    {
        mmr_max=-1.0;
        for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        {
            docInd_it=docInd.find(dS_R_it->first);
            index_R=(int)docInd_it->second;
            docID1=index_->document(dS_R_it->first);
            docLen1=index_->docLength(docID1);
            sim2_max=0.0;
            for(dS_S_it=dS_S.begin(); dS_S_it!=dS_S.end(); dS_S_it++)
            {
                docInd_it=docInd.find(dS_S_it->first);
                index_S=(int)docInd_it->second;
                docID2=index_->document(dS_S_it->first);
                docLen2=index_->docLength(docID2);
                sum1=0.0;
                for(tF_it1=dTF[index_R].begin(); tF_it1!=dTF[index_R].end(); tF_it1++)
                {
                    termID=index_->term(tF_it1->first);
                    smoPro1=(tF_it1->second+this->smoPar_*collModel_->prob(termID))/(docLen1+this->smoPar_);
                    tF_it2=dTF[index_S].find(tF_it1->first);
                    if(tF_it2==dTF[index_S].end())
                    {
                        smoPro2=this->smoPar_*collModel_->prob(termID)/(docLen2+this->smoPar_);
                    }
                    else
                    {
                        smoPro2=(tF_it2->second+this->smoPar_*collModel_->prob(termID))/(docLen2+this->smoPar_);
                    }
                    sum1=sum1+smoPro1*log(smoPro1/smoPro2);
                }//for(tF_it1=dTF[index_R].begin(); tF_it1!=dTF[index_R].end(); tF_it1++)
                sum2=0.0;
                for(tF_it2=dTF[index_S].begin(); tF_it2!=dTF[index_S].end(); tF_it2++)
                {
                    termID=index_->term(tF_it2->first);
                    smoPro2=(tF_it2->second+this->smoPar_*collModel_->prob(termID))/(docLen2+this->smoPar_);
                    tF_it1=dTF[index_R].find(tF_it2->first);
                    if(tF_it1==dTF[index_R].end())
                    {
                        smoPro1=this->smoPar_*collModel_->prob(termID)/(docLen1+this->smoPar_);
                    }
                    else
                    {
                        smoPro1=(tF_it1->second+this->smoPar_*collModel_->prob(termID))/(docLen1+this->smoPar_);
                    }
                    sum2=sum2+smoPro2*log(smoPro2/smoPro1);
                }
                sim2=exp(-0.5*(sum1+sum2));
                if(sim2>sim2_max)
                {
                    sim2_max=sim2;
                }
            }//for(dS_S_it=dS_S.begin(); dS_S_it!=dS_S.end(); dS_S_it++)
            sim1=dS_R_it->second/fusSco_max;
            mmr=lambdaMMR_*(sim1 - (1.0-lambdaMMR_)*sim2_max);
            if(mmr_max<mmr)
            {
                mmr_max=mmr;
                docStr=dS_R_it->first;
                //docInd_it=docInd.find(dS_R_it->first);
            }
        }//for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        dS_S.insert(lss::DOCSCO_PAIR_T(docStr, numOfDoc-i));
        dS_R.erase(docStr);
    }//for(int i=0; i<numOfDoc; i++)
    this->qDS.insert(lss::QUEDOCSCO_PAIR_T(que, dS_S));
}*/

/*
 double
 lss::ManifoldCluster::computeKLScor(std::string doc1, std::string doc2)
 {
 int docID1=index_->document(doc1);
 int docID2=index_->document(doc2);
 double docLen1=index_->docLength(docID1);
 double docLen2=index_->docLength(docID2);
 double skLScor=0.0;//symmetric K-L divergence score
 
 lemur::langmod::DocUnigramCounter* collCounter1
 = new lemur::langmod::DocUnigramCounter(docID1,*index_ );
 lemur::langmod::DocUnigramCounter* collCounter2
 = new lemur::langmod::DocUnigramCounter(docID2, *index_);
 lemur::langmod::DocUnigramCounter* collCounter
 =new lemur::langmod::DocUnigramCounter(*index_);
 
 lemur::langmod::UnigramLM* collModel1
 = new lemur::langmod::MLUnigramLM( *collCounter1, index_->termLexiconID() );
 lemur::langmod::UnigramLM *collModel2
 = new lemur::langmod::MLUnigramLM( *collCounter2, index_->termLexiconID() );
 lemur::langmod::UnigramLM *collModel
 = new lemur::langmod::MLUnigramLM( *collCounter, index_->termLexiconID() );
 
 int termID=0;
 double prob=0.0;
 double probSum=0.0;
 double smoPro1=0.0;//smoothing p(w|Q)
 double smoPro2=0.0;//smoothing p(w|D)
 double muPwC=0.0;//mu*p(w|c)
 double sumWInQ=0.0;//term in Q
 double sumWInDNotInQ=0.0;//term in D but not in Q
 double sumWOutQAndD=0.0;// term in the collection, but not in both Q and D (outside both Q and D)
 std::string termStr;
 collModel1->startIteration();
 for(;collModel1->hasMore();)
 {
 collModel1->nextWordProb(termID, prob);
 muPwC=this->smoPar_*collModel->prob(termID);
 if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
 {
 smoPro1=(prob*docLen1+muPwC)/(docLen1+this->smoPar_);
 smoPro2=(collModel2->prob(termID)*docLen2+muPwC)/(docLen2+this->smoPar_);
 sumWInQ=sumWInQ+smoPro1*(log(smoPro1/smoPro2));
 }
 }
 skLScor=sumWInQ;
 collModel2->startIteration();
 for(;collModel2->hasMore();)
 {
 collModel2->nextWordProb(termID, prob);
 muPwC=this->smoPar_*collModel->prob(termID);
 if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
 {
 smoPro1=(collModel1->prob(termID)*docLen1+muPwC)/(docLen1+this->smoPar_);
 smoPro2=(prob*docLen2+muPwC)/(docLen2+this->smoPar_);
 sumWInDNotInQ=sumWInDNotInQ+smoPro2*log(smoPro2/smoPro1);
 }
 }
 skLScor=skLScor+sumWInDNotInQ;
 delete collCounter1;
 collCounter1=0;
 delete collCounter2;
 collCounter2=0;
 delete collModel1;
 collModel1=0;
 delete collModel2;
 collModel2=0;
 return 0.5*skLScor;
 }
 */




