//
//  MP2.h
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 10/5/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#ifndef _MP2LSS_H
#define _MP2LSS_H

#include "DataTypesTwo.h"
#include "Processing.h"
#include "ResultWriter.h"
#include <iostream>
#include <iterator>
#include <algorithm>

namespace lss
{
    class MP2
    {
    public:
        MP2(const lemur::api::Index& index): index_(index){};
        void processMP(const lss::TWODIM_T docTopArr, const std::vector<double> pTopic, const std::string que, lss::DOCSCO_T docSco); //docTopArr: document-topic distribution size M x K; topVal: p(z); docSco: document-fusion score
        void retMod_tim(); // the structure of the data must be qID/date/filNam, for time aware diversification in short text stream
        void retMod_who(); // for diversification in short text stream, the kdd 2014 paper
        void retMod_lan(); // for language model in short text stream, Jelinek-Mercer language model
        lss::DOCSCO_T processMP2_tim(lss::STRSCOVEC_T docScoVec, const std::vector<double> pTopic, const std::string que, lss::DOCSCO_T docSco); //MP2 for time aware diversification in short text stream
        lss::QUEDOCSCO_T getQDS();
    protected:
        const lemur::api::Index& index_;
    private:
        int topK_;
        lss::ResultWriter resWri_;
        lss::QUEDOCSCO_T qDS;
    };
}

#endif
