//
//  MRR.h
//  diversify
//
//  Created by Shangsong Liang on 7/23/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#ifndef _MRRLSS_H
#define _MRRLSS_H

#include "DataTypesTwo.h"
#include "Processing.h"
#include "strtokenizer.h"
#include "ResultWriter.h"

//Lemur
#include "Index.hpp"
#include "Param.hpp"
#include "LemurIndriIndex.hpp"
#include "IndexManager.hpp"
#include "DocumentRep.hpp"
#include "DocUnigramCounter.hpp"
#include "UnigramLM.hpp"

#include "SimpleKLRetMethod.hpp"
#include "common_headers.hpp"
#include "DocUnigramCounter.hpp"
#include "RelDocUnigramCounter.hpp"
#include "OneStepMarkovChain.hpp"

namespace lss
{
    class MRR
    {
    public:
        MRR(const lemur::api::Index& index): index_(index){};
        //MRR();
        ~MRR();
        void retMod(); //retrieval model
        void processMRR(const std::string que, lss::DOCSCO_T docSco); // return top-k document with ranked score using index
        lss::DOCSCO_T processMRR_wor(const std::string que, lss::DOCSCO_T dS); // return top-k document with ranked score using document directly
        void processMRR0(const std::string que, lss::DOCSCO_T docSco);
        void processMRRUsingTex(const std::string que, lss::DOCSCO_T docSco); // use document text to get the mrr result
        lss::QUEDOCSCO_T getQDS();
    private:
        lss::QUEDOCSCO_T qDS;
        const lemur::api::Index* indexP_;
        //const lemur::api::Index& index_;
        //const lemur::api::Index* index_;
        lemur::langmod::DocUnigramCounter* collCounter_;
        lemur::langmod::UnigramLM *collModel_;
        //void openIndex();
        //void closeIndex();
        double smoPar_;
        double lambdaMMR_;
        int topK_;
        std::string smoMet_;
        lss::ResultWriter resWri_;
    protected:
        const lemur::api::Index& index_;
    };
}

#endif