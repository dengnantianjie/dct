//
//  JMRetrievalModel.h
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/4/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#ifndef DivStr_JMRetrievalModel_h
#define DivStr_JMRetrievalModel_h

#include "DataTypesTwo.h"

#include "Association.h"
#include <iomanip>


namespace lss{
    class JMRetrievalModel : public Association
    {
    public:
        JMRetrievalModel(const lemur::api::Index& index, const lss::QueryRep& query) : Association(index, query){};
        void init();
        virtual lss::QUEDOCSCO_T getQueDocSco();
        void wriQueDocScoToFil(); //write query document scores to files
    private:
        void scoreCollection( const int& qID, lemur::api::IndexedRealVector& pqd_all );
        /// Smooth support file
        std::string smoothSupportFile_;
        int topDocNum_;
    };
}


#endif
