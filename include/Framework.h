//
//  Framework.h
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/4/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#ifndef DivStr_Framework_h
#define DivStr_Framework_h

#include "DataTypesTwo.h"
#include "Param.hpp"
#include "dirent.h"
#include "QueryRep.h"
#include "MRR.h"
#include "MP2.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <cmath>

// Lemur
#include "Param.hpp"
#include "Index.hpp"
#include "LemurIndriIndex.hpp"
#include "IndexManager.hpp"
#include "DocumentRep.hpp"
#include "DocUnigramCounter.hpp"
#include "UnigramLM.hpp"

#include "SimpleKLRetMethod.hpp"
#include "common_headers.hpp"
#include "DocUnigramCounter.hpp"
#include "RelDocUnigramCounter.hpp"
#include "OneStepMarkovChain.hpp"

namespace lss{
    class Framework
    {
    public:
        Framework() {};
        ~Framework() {};
        void run();
        void show_help();
    private:
        const lemur::api::Index* index_;
        lss::QueryRep* queries_;
    };
}

#endif








